(* ************************************************************************** *)
(*                                                                            *)
(*                                                        :::      ::::::::   *)
(*   crossover.ml                                       :+:      :+:    :+:   *)
(*                                                    +:+ +:+         +:+     *)
(*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        *)
(*                                                +#+#+#+#+#+   +#+           *)
(*   Created: 2016/01/19 20:53:57 by cpestour          #+#    #+#             *)
(*   Updated: 2016/01/19 21:01:39 by cpestour         ###   ########.fr       *)
(*                                                                            *)
(* ************************************************************************** *)

let rev_list lst =
	let rec rev_aux l1 l2 =	match l1 with
	| [] -> l2
	| h::l -> rev_aux l (h::l2)
	in
	rev_aux lst []

let rec is_inside x = function
	| [] -> false
	| h::l -> if h = x then true else is_inside x l

let crossover l1 l2 =
	if l1 = [] || l2 = [] then []
	else
		let rec cros_aux acc = function
			| [] -> rev_list acc
			| h::l -> if is_inside h l2 then cros_aux (h::acc) l
				else cros_aux acc l
		in
		cros_aux [] l1
