(* ************************************************************************** *)
(*                                                                            *)
(*                                                        :::      ::::::::   *)
(*   nucleotides.ml                                     :+:      :+:    :+:   *)
(*                                                    +:+ +:+         +:+     *)
(*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        *)
(*                                                +#+#+#+#+#+   +#+           *)
(*   Created: 2016/01/19 22:16:06 by cpestour          #+#    #+#             *)
(*   Updated: 2016/01/19 22:32:19 by cpestour         ###   ########.fr       *)
(*                                                                            *)
(* ************************************************************************** *)

type phosphate = string

type deoxyribose = string

type nucleobase = A | T | C | G | None

type nucleotide =
{
	phos: phosphate;
	deox: deoxyribose;
	nucl: nucleobase
}

let generate_nucleotide = function
	| 'a' | 'A' -> {phos = "phosphate"; deox = "deoxyribose"; nucl = A}
	| 't' | 'T' -> {phos = "phosphate"; deox = "deoxyribose"; nucl = T}
	| 'c' | 'C' -> {phos = "phosphate"; deox = "deoxyribose"; nucl = C}
	| 'g' | 'G' -> {phos = "phosphate"; deox = "deoxyribose"; nucl = G}
	| _ -> {phos = "phosphate"; deox = "deoxyribose"; nucl = None}
