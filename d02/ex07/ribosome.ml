(* ************************************************************************** *)
(*                                                                            *)
(*                                                        :::      ::::::::   *)
(*   ribosome.ml                                        :+:      :+:    :+:   *)
(*                                                    +:+ +:+         +:+     *)
(*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        *)
(*                                                +#+#+#+#+#+   +#+           *)
(*   Created: 2016/01/20 00:01:27 by cpestour          #+#    #+#             *)
(*   Updated: 2016/01/20 21:21:39 by cpestour         ###   ########.fr       *)
(*                                                                            *)
(* ************************************************************************** *)

type phosphate = string
type deoxyribose = string
type nucleobase = A | T | C | G | U | None
type nucleotide =
{
	phos: phosphate;
	deox: deoxyribose;
	nucl: nucleobase
}
type helix = nucleotide list
type rna = nucleobase list
type aminoacid = Stop | Ala | Arg | Asn | Asp | Cys | Gln | Glu | Gly | His
				 | Ile | Leu | Lys | Met | Phe | Pro | Ser | Thr | Trp | Tyr | Val | None
type protein = aminoacid list

let generate_nucleotide = function
	| 'a' | 'A' -> {phos = "phosphate"; deox = "deoxyribose"; nucl = A}
	| 't' | 'T' -> {phos = "phosphate"; deox = "deoxyribose"; nucl = T}
	| 'c' | 'C' -> {phos = "phosphate"; deox = "deoxyribose"; nucl = C}
	| 'g' | 'G' -> {phos = "phosphate"; deox = "deoxyribose"; nucl = G}
	| _ -> {phos = "phosphate"; deox = "deoxyribose"; nucl = None}

let rand_char = function
	| 0 -> 'a'
	| 1 -> 't'
	| 2 -> 'c'
	| _ -> 'g'

let generate_helix : int -> helix = fun n ->
	let rec loop i lst =
		if i = n then lst
		else
			begin
				Random.self_init ();
				loop (i + 1) (lst @ ((generate_nucleotide (rand_char (Random.int 4)))::[]))
			end
	in
	loop 0 []

let nucl_to_string = function
	| A -> "A"
	| T -> "T"
	| C -> "C"
	| G -> "G"
	| _ -> "None"

let helix_to_string : helix -> string = fun lst ->
	let rec loop str = function
		| [] -> str
		| h::l -> loop (str ^ (nucl_to_string h.nucl)) l
	in
	loop "" lst

let get_comp = function
	| A -> 't'
	| T -> 'a'
	| C -> 'g'
	| G -> 'c'
	| _ -> 'b'

let complementary_helix : helix -> helix = fun hel ->
	let rec loop lst = function
		| [] -> lst
		| h::l -> loop (lst @ ((generate_nucleotide (get_comp h.nucl))::[])) l
	in
	loop [] hel

let get_comp_rna = function
	| A -> U
	| T -> A
	| C -> G
	| G -> C
	| _ -> None

let generate_rna : helix -> rna = fun hel ->
	let rec loop lst = function
		| [] -> lst
		| h::l -> loop (lst @ ((get_comp_rna h.nucl)::[])) l
	in
	loop [] hel

let generate_bases_triplets : rna -> (nucleobase * nucleobase * nucleobase) list = fun r ->
	let rec loop lst = function
		| [] | _::[] | _::_::[] -> lst
		| h1::h2::h3::l -> loop (lst @ ((h1, h2, h3)::[])) l
	in
	loop [] r

let string_of_amino = function
	| Stop -> "End of translation"
	| Ala -> "Alanine"
	| Arg -> "Arginine"
	| Asn -> "Aparagine"
	| Asp -> "Aspartique"
	| Cys -> "Cysteine"
	| Gln -> "Glutamine"
	| Glu -> "Glutamique"
	| Gly -> "Glycine"
	| His -> "Histidine"
	| Ile -> "Isoleucine"
	| Leu -> "Leucine"
	| Lys -> "Lysine"
	| Met -> "Methionine"
	| Phe -> "Phenylalanine"
	| Pro -> "Proline"
	| Ser -> "Serine"
	| Thr -> "Threonine"
	| Trp -> "Tryptophane"
	| Tyr -> "Tyrosine"
	| Val -> "Valine"
	| _ -> "None"

let string_of_protein : protein -> string = fun p ->
	let rec loop str = function
		| [] -> str
		| h::l -> if str = "" then
				loop (string_of_amino h) l
			else
				loop (str ^ " " ^ (string_of_amino h)) l
	in
	loop "" p

let decode_arn : rna -> protein = fun r ->
	let rec loop = function
		| [] -> []
		| (U, A, A)::_ | (U, A, G)::_ | (U, G, A)::_ -> Stop::[]
		| (G, C, _)::l -> Ala::(loop l)
		| (A, G, A)::l | (A, G, G)::l | (C, G, _)::l -> Arg::(loop l)
		| (A, A, C)::l | (A, A, U)::l -> Asn::(loop l)
		| (G, A, C)::l | (G, A, U)::l -> Asp::(loop l)
		| (U, G, C)::l | (U, G, U)::l -> Cys::(loop l)
		| (C, A, A)::l | (C, A, G)::l -> Gln::(loop l)
		| (G, A, A)::l | (G, A, G)::l -> Glu::(loop l)
		| (G, G, _)::l -> Gly::(loop l)
		| (C, A, C)::l | (C, A, U)::l -> His::(loop l)
		| (A, U, A)::l | (A, U, C)::l | (A, U, U)::l -> Ile::(loop l)
		| (C, U, _)::l | (U, U, A)::l | (U, U, G)::l -> Leu::(loop l)
		| (A, A, A)::l | (A, A, G)::l -> Lys::(loop l)
		| (A, U, G)::l -> Met::(loop l)
		| (U, U, C)::l | (U, U, U)::l -> Phe::(loop l)
		| (C, C, _)::l -> Pro::(loop l)
		| (U, C, _)::l | (A, G, U)::l | (A, G, C)::l -> Ser::(loop l)
		| (A, C, _)::l -> Thr::(loop l)
		| (U, G, G)::l -> Trp::(loop l)
		| (U, A, C)::l | (U, A, U)::l -> Tyr::(loop l)
		| (G, U, _)::l -> Val::(loop l)
		| _ -> []
	in loop (generate_bases_triplets r)
