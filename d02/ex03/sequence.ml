(* ************************************************************************** *)
(*                                                                            *)
(*                                                        :::      ::::::::   *)
(*   sequence.ml                                        :+:      :+:    :+:   *)
(*                                                    +:+ +:+         +:+     *)
(*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        *)
(*                                                +#+#+#+#+#+   +#+           *)
(*   Created: 2016/01/19 21:51:53 by cpestour          #+#    #+#             *)
(*   Updated: 2016/01/19 22:14:16 by cpestour         ###   ########.fr       *)
(*                                                                            *)
(* ************************************************************************** *)

let rec lst_to_string lst str = match lst with
	| [] -> str
	| h::[] -> str ^ (string_of_int h)
	| h::l -> lst_to_string l (str ^ (string_of_int h))

let rec get_lst lst nb acc = match lst with
	| [] -> acc
	| h::[] -> acc @ (nb::h::[])
	| h::n::l -> if h = n then
			get_lst (n::l) (nb + 1) acc
		else
			get_lst (n::l) 1 (acc @ (nb::h::[]))

let sequence n =
	if n <= 0 then ""
	else
		let rec loop lst i =
			if i = n then lst
			else loop (get_lst lst 1 []) (i + 1)
		in
		lst_to_string (loop (1::[]) 1) ""
