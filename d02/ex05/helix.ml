(* ************************************************************************** *)
(*                                                                            *)
(*                                                        :::      ::::::::   *)
(*   helix.ml                                           :+:      :+:    :+:   *)
(*                                                    +:+ +:+         +:+     *)
(*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        *)
(*                                                +#+#+#+#+#+   +#+           *)
(*   Created: 2016/01/19 22:32:05 by cpestour          #+#    #+#             *)
(*   Updated: 2016/01/19 23:02:10 by cpestour         ###   ########.fr       *)
(*                                                                            *)
(* ************************************************************************** *)

type phosphate = string

type deoxyribose = string

type nucleobase = A | T | C | G | None

type nucleotide =
{
	phos: phosphate;
	deox: deoxyribose;
	nucl: nucleobase
}

type helix = nucleotide list

let generate_nucleotide = function
	| 'a' | 'A' -> {phos = "phosphate"; deox = "deoxyribose"; nucl = A}
	| 't' | 'T' -> {phos = "phosphate"; deox = "deoxyribose"; nucl = T}
	| 'c' | 'C' -> {phos = "phosphate"; deox = "deoxyribose"; nucl = C}
	| 'g' | 'G' -> {phos = "phosphate"; deox = "deoxyribose"; nucl = G}
	| _ -> {phos = "phosphate"; deox = "deoxyribose"; nucl = None}

let rand_char = function
	| 0 -> 'a'
	| 1 -> 't'
	| 2 -> 'c'
	| 3 -> 'g'
	| _ -> 'b'

let generate_helix : int -> helix = fun n ->
	let rec loop i lst =
		if i = n then lst
		else
			begin
				Random.self_init ();
				loop (i + 1) (lst @ ((generate_nucleotide (rand_char (Random.int 5)))::[]))
			end
	in
	loop 0 []

let nucl_to_string = function
	| A -> "A"
	| T -> "T"
	| C -> "C"
	| G -> "G"
	| _ -> "None"

let helix_to_string : helix -> string = fun lst ->
	let rec loop str = function
		| [] -> str
		| h::l -> loop (str ^ (nucl_to_string h.nucl)) l
	in
	loop "" lst

let get_comp = function
	| A -> 't'
	| T -> 'a'
	| C -> 'g'
	| G -> 'c'
	| _ -> 'b'

let complementary_helix : helix -> helix = fun hel ->
	let rec loop lst = function
		| [] -> lst
		| h::l -> loop (lst @ ((generate_nucleotide (get_comp h.nucl))::[])) l
	in
	loop [] hel
