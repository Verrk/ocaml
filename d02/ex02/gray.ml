(* ************************************************************************** *)
(*                                                                            *)
(*                                                        :::      ::::::::   *)
(*   gray.ml                                            :+:      :+:    :+:   *)
(*                                                    +:+ +:+         +:+     *)
(*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        *)
(*                                                +#+#+#+#+#+   +#+           *)
(*   Created: 2016/01/19 21:12:21 by cpestour          #+#    #+#             *)
(*   Updated: 2016/01/19 21:46:53 by cpestour         ###   ########.fr       *)
(*                                                                            *)
(* ************************************************************************** *)

let get_gray i = i lxor (i lsr 1)

let get_bin i size =
	let str = String.make size '0' in
	let rec bin_aux idx bit =
		if (bit land 1) = 1 then String.set str idx '1';
		if idx <= 0 then str
		else bin_aux (idx - 1) (bit lsr 1)
	in
	bin_aux (size - 1) i

let rec ft_power n p = match p with
	| 0 -> 1
	| p when p > 0 -> n * ft_power n (p - 1)
	| _ -> 0

let gray i =
	let rec print_one acc =
		if acc < (ft_power 2 i) then
			begin
				print_string (get_bin (get_gray acc) i);
				if (acc + 1) = (ft_power 2 i) then print_char '\n' else print_char ' ';
				print_one (acc + 1)
			end
	in
	print_one 0
