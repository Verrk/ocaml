(* ************************************************************************** *)
(*                                                                            *)
(*                                                        :::      ::::::::   *)
(*   encode.ml                                          :+:      :+:    :+:   *)
(*                                                    +:+ +:+         +:+     *)
(*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        *)
(*                                                +#+#+#+#+#+   +#+           *)
(*   Created: 2016/01/19 20:33:02 by cpestour          #+#    #+#             *)
(*   Updated: 2016/01/19 20:59:12 by cpestour         ###   ########.fr       *)
(*                                                                            *)
(* ************************************************************************** *)

let rev_list lst =
	let rec rev_aux l1 l2 =	match l1 with
	| [] -> l2
	| h::l -> rev_aux l (h::l2)
	in
	rev_aux lst []

let encode lst =
	let rec enc_aux l nb acc = match l with
		| [] -> []
		| h::[] -> (nb, h)::acc
		| h::n::l -> if h = n then
				enc_aux (n::l) (nb + 1) acc
			else
				enc_aux (n::l) 1 ((nb, h)::acc)
	in
	rev_list (enc_aux lst 1 [])
