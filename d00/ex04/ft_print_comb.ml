(* ************************************************************************** *)
(*                                                                            *)
(*                                                        :::      ::::::::   *)
(*   ft_print_comb.ml                                   :+:      :+:    :+:   *)
(*                                                    +:+ +:+         +:+     *)
(*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        *)
(*                                                +#+#+#+#+#+   +#+           *)
(*   Created: 2016/01/17 21:03:03 by cpestour          #+#    #+#             *)
(*   Updated: 2016/01/17 22:03:16 by cpestour         ###   ########.fr       *)
(*                                                                            *)
(* ************************************************************************** *)

let ft_print_comb () =
	let rec loop i j k =
		if k <= 9 then
			begin
				print_int i;
				print_int j;
				print_int k;
				if i = 7 then
					print_string "\n"
				else
					begin
						print_string ", ";
						loop i j (k + 1)
					end
			end
		else
			if j < 8 then
				loop i (j + 1) (j + 2)
			else if i < 7 then
				loop (i + 1) (i + 2) (i + 3)
	in
	loop 0 1 2
