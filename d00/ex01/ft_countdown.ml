(* ************************************************************************** *)
(*                                                                            *)
(*                                                        :::      ::::::::   *)
(*   ft_countdown.ml                                    :+:      :+:    :+:   *)
(*                                                    +:+ +:+         +:+     *)
(*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        *)
(*                                                +#+#+#+#+#+   +#+           *)
(*   Created: 2016/01/17 20:37:06 by cpestour          #+#    #+#             *)
(*   Updated: 2016/01/17 20:44:35 by cpestour         ###   ########.fr       *)
(*                                                                            *)
(* ************************************************************************** *)

let rec ft_countdown x =
	if x <= 0 then
		begin
			print_int 0;
			print_char '\n'
		end
	else
		begin
			print_int x;
			print_char '\n';
			ft_countdown (x-1)
		end
