(* ************************************************************************** *)
(*                                                                            *)
(*                                                        :::      ::::::::   *)
(*   ft_print_comb2.ml                                  :+:      :+:    :+:   *)
(*                                                    +:+ +:+         +:+     *)
(*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        *)
(*                                                +#+#+#+#+#+   +#+           *)
(*   Created: 2016/01/17 21:59:46 by cpestour          #+#    #+#             *)
(*   Updated: 2016/01/17 22:05:01 by cpestour         ###   ########.fr       *)
(*                                                                            *)
(* ************************************************************************** *)

let ft_print_int i =
	if i < 10 then
		print_int 0;
	print_int i

let ft_print_comb2 () =
	let rec loop i j =
		if j <= 99 then
			begin
				ft_print_int i;
				print_char ' ';
				ft_print_int j;
				if i = 98 then
					print_char '\n'
				else
					begin
						print_char ',';
						print_char ' ';
						loop i (j + 1)
					end
			end
		else
			if i < 98 then
				loop (i + 1) (i + 2)
	in
	loop 0 1
