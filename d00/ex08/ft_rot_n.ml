(* ************************************************************************** *)
(*                                                                            *)
(*                                                        :::      ::::::::   *)
(*   ft_rot_n.ml                                        :+:      :+:    :+:   *)
(*                                                    +:+ +:+         +:+     *)
(*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        *)
(*                                                +#+#+#+#+#+   +#+           *)
(*   Created: 2016/01/17 21:42:39 by cpestour          #+#    #+#             *)
(*   Updated: 2016/01/22 18:59:46 by cpestour         ###   ########.fr       *)
(*                                                                            *)
(* ************************************************************************** *)

let is_lower c = c >= 'a' && c <= 'z'

let is_upper c = c >= 'A' && c <= 'Z'

let rot c =
	if is_lower c then
		char_of_int((int_of_char(c) - int_of_char('a') + 1) mod 26 + int_of_char('a'))
	else if is_upper c then
		char_of_int((int_of_char(c) - int_of_char('A') + 1) mod 26 + int_of_char('A'))
	else
		c


let rec ft_rot_n n str =
	if n > 0 then
		ft_rot_n (n - 1) (String.map rot str)
	else
		str

