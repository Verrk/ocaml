(* ************************************************************************** *)
(*                                                                            *)
(*                                                        :::      ::::::::   *)
(*   ft_is_palindrome.ml                                :+:      :+:    :+:   *)
(*                                                    +:+ +:+         +:+     *)
(*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        *)
(*                                                +#+#+#+#+#+   +#+           *)
(*   Created: 2016/01/17 21:34:23 by cpestour          #+#    #+#             *)
(*   Updated: 2016/01/17 21:36:10 by cpestour         ###   ########.fr       *)
(*                                                                            *)
(* ************************************************************************** *)

let ft_is_palindrome s =
	let rec loop i j =
		if i < j then
			if String.get s i <> String.get s j then
				false
			else
				loop (i + 1) (j - 1)
		else
			true
	in
	loop 0 ((String.length s) - 1)
