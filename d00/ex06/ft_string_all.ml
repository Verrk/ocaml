(* ************************************************************************** *)
(*                                                                            *)
(*                                                        :::      ::::::::   *)
(*   ft_string_all.ml                                   :+:      :+:    :+:   *)
(*                                                    +:+ +:+         +:+     *)
(*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        *)
(*                                                +#+#+#+#+#+   +#+           *)
(*   Created: 2016/01/17 21:28:46 by cpestour          #+#    #+#             *)
(*   Updated: 2016/01/17 21:31:20 by cpestour         ###   ########.fr       *)
(*                                                                            *)
(* ************************************************************************** *)

let ft_string_all f s =
	let rec loop i =
		if i < String.length s then
			if f (String.get s i) = false then
				false
			else
				loop (i + 1)
		else
			true
	in
	loop 0
