(* ************************************************************************** *)
(*                                                                            *)
(*                                                        :::      ::::::::   *)
(*   ft_print_alphabet.ml                               :+:      :+:    :+:   *)
(*                                                    +:+ +:+         +:+     *)
(*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        *)
(*                                                +#+#+#+#+#+   +#+           *)
(*   Created: 2016/01/17 20:52:06 by cpestour          #+#    #+#             *)
(*   Updated: 2016/01/17 20:59:17 by cpestour         ###   ########.fr       *)
(*                                                                            *)
(* ************************************************************************** *)

let ft_print_alphabet () =
	let rec loop c =
		if char_of_int (c) <= 'z' then
			begin
				print_char (char_of_int (c) );
				loop (c + 1)
			end
	in
	loop (int_of_char ('a') );
	print_char '\n'

