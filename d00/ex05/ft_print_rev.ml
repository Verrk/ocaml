(* ************************************************************************** *)
(*                                                                            *)
(*                                                        :::      ::::::::   *)
(*   ft_print_rev.ml                                    :+:      :+:    :+:   *)
(*                                                    +:+ +:+         +:+     *)
(*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        *)
(*                                                +#+#+#+#+#+   +#+           *)
(*   Created: 2016/01/17 21:20:25 by cpestour          #+#    #+#             *)
(*   Updated: 2016/01/17 21:25:33 by cpestour         ###   ########.fr       *)
(*                                                                            *)
(* ************************************************************************** *)

let ft_print_rev s =
	let rec loop i =
		if i >= 0 then
			begin
				print_char (String.get s i);
				loop (i - 1)
			end
		else
			print_char '\n'
	in
	loop ((String.length s) - 1)
