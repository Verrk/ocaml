(* ************************************************************************** *)
(*                                                                            *)
(*                                                        :::      ::::::::   *)
(*   repeat_x.ml                                        :+:      :+:    :+:   *)
(*                                                    +:+ +:+         +:+     *)
(*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        *)
(*                                                +#+#+#+#+#+   +#+           *)
(*   Created: 2016/01/17 22:18:38 by cpestour          #+#    #+#             *)
(*   Updated: 2016/01/17 22:22:06 by cpestour         ###   ########.fr       *)
(*                                                                            *)
(* ************************************************************************** *)

let repeat_x n =
	if n < 0 then
		"Error"
	else
		begin
			let rec loop i str =
				if i > 0 then
					loop (i - 1) (str ^ "x")
				else
					str
			in
			loop n ""
		end
