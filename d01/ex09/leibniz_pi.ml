(* ************************************************************************** *)
(*                                                                            *)
(*                                                        :::      ::::::::   *)
(*   leibniz_pi.ml                                      :+:      :+:    :+:   *)
(*                                                    +:+ +:+         +:+     *)
(*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        *)
(*                                                +#+#+#+#+#+   +#+           *)
(*   Created: 2016/01/18 19:33:19 by cpestour          #+#    #+#             *)
(*   Updated: 2016/01/18 19:41:16 by cpestour         ###   ########.fr       *)
(*                                                                            *)
(* ************************************************************************** *)

let leibniz_pi delta =
	let piref = 4. *. atan 1. in
	let rec leib_aux acc n =
		if delta >= (piref -. acc) && piref >= acc || delta >= (acc -. piref) && acc >= piref then n
		else
			let sum = (-1.)**(float_of_int n) /. (2. *. (float_of_int n) +. 1.) in
			leib_aux (acc +. (4. *. sum)) (n + 1)
	in
	leib_aux 0. 0
