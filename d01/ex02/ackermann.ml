(* ************************************************************************** *)
(*                                                                            *)
(*                                                        :::      ::::::::   *)
(*   ackermann.ml                                       :+:      :+:    :+:   *)
(*                                                    +:+ +:+         +:+     *)
(*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        *)
(*                                                +#+#+#+#+#+   +#+           *)
(*   Created: 2016/01/17 23:17:21 by cpestour          #+#    #+#             *)
(*   Updated: 2016/01/17 23:31:06 by cpestour         ###   ########.fr       *)
(*                                                                            *)
(* ************************************************************************** *)

let rec ackermann m n =
	if m < 0 || n < 0 then -1
	else if m = 0 then (n + 1)
	else if n = 0 then ackermann (m - 1) 1
	else ackermann (m - 1) (ackermann m (n - 1))
