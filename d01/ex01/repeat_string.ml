(* ************************************************************************** *)
(*                                                                            *)
(*                                                        :::      ::::::::   *)
(*   repeat_string.ml                                   :+:      :+:    :+:   *)
(*                                                    +:+ +:+         +:+     *)
(*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        *)
(*                                                +#+#+#+#+#+   +#+           *)
(*   Created: 2016/01/17 23:09:22 by cpestour          #+#    #+#             *)
(*   Updated: 2016/01/17 23:15:05 by cpestour         ###   ########.fr       *)
(*                                                                            *)
(* ************************************************************************** *)

let repeat_string ?(str="x") n =
	if n < 0 then
		"Error"
	else
		begin
			let rec loop i s =
				if i > 0 then
					loop (i - 1) (s ^ str)
				else
					s
			in
			loop n ""
		end
