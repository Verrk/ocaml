(* ************************************************************************** *)
(*                                                                            *)
(*                                                        :::      ::::::::   *)
(*   hofstadter_mf.ml                                   :+:      :+:    :+:   *)
(*                                                    +:+ +:+         +:+     *)
(*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        *)
(*                                                +#+#+#+#+#+   +#+           *)
(*   Created: 2016/01/18 18:53:29 by cpestour          #+#    #+#             *)
(*   Updated: 2016/01/18 18:56:15 by cpestour         ###   ########.fr       *)
(*                                                                            *)
(* ************************************************************************** *)

let rec hfs_f n =
	if n < 0 then -1
	else if n = 0 then 1
	else n - hfs_m (hfs_f (n - 1))
and hfs_m n =
	if n < 0 then -1
	else if n = 0 then 0
	else n - hfs_f (hfs_m (n - 1))
