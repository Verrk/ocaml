(* ************************************************************************** *)
(*                                                                            *)
(*                                                        :::      ::::::::   *)
(*   ft_sum.ml                                          :+:      :+:    :+:   *)
(*                                                    +:+ +:+         +:+     *)
(*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        *)
(*                                                +#+#+#+#+#+   +#+           *)
(*   Created: 2016/01/18 19:10:12 by cpestour          #+#    #+#             *)
(*   Updated: 2016/01/18 19:20:44 by cpestour         ###   ########.fr       *)
(*                                                                            *)
(* ************************************************************************** *)

let ft_sum f l u =
	let rec sum_aux i c =
		if c = u then i
		else sum_aux (i +. f (c + 1)) (c + 1)
	in
	sum_aux (f l) l
