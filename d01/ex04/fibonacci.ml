(* ************************************************************************** *)
(*                                                                            *)
(*                                                        :::      ::::::::   *)
(*   fibonacci.ml                                       :+:      :+:    :+:   *)
(*                                                    +:+ +:+         +:+     *)
(*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        *)
(*                                                +#+#+#+#+#+   +#+           *)
(*   Created: 2016/01/17 23:51:43 by cpestour          #+#    #+#             *)
(*   Updated: 2016/01/18 00:01:04 by cpestour         ###   ########.fr       *)
(*                                                                            *)
(* ************************************************************************** *)

let fibonacci n =
	if n < 0 then -1
	else
		let rec fibo_aux a b n =
			if n > 0 then
				fibo_aux b (a + b) (n - 1)
			else
				a
		in
		fibo_aux 0 1 n
