(* ************************************************************************** *)
(*                                                                            *)
(*                                                        :::      ::::::::   *)
(*   gardening.ml                                       :+:      :+:    :+:   *)
(*                                                    +:+ +:+         +:+     *)
(*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        *)
(*                                                +#+#+#+#+#+   +#+           *)
(*   Created: 2016/01/20 22:44:01 by cpestour          #+#    #+#             *)
(*   Updated: 2016/01/20 23:26:59 by cpestour         ###   ########.fr       *)
(*                                                                            *)
(* ************************************************************************** *)

type 'a tree = Nil | Node of 'a * 'a tree * 'a tree

let draw_square x y size =
	Graphics.moveto (x - size / 2) (y - size / 2);
	Graphics.lineto (x - size / 2 + size) (y - size / 2);
	Graphics.lineto (x - size / 2 + size) (y - size / 2 + size);
	Graphics.lineto (x - size / 2) (y - size / 2 + size);
	Graphics.lineto (x - size / 2) (y - size / 2)

let string_of_node = function
	| Nil -> "Nil"
	| Node(v, _, _) -> v

let size tree =
	let rec loop = function
		| Nil -> 0
		| Node(_, l, r) -> (loop  l) + (loop  r) + 1
	in
	loop tree

let heigth tree =
	let rec loop = function
		| Nil -> 0
		| Node(_, l, r) -> 1 + max (loop l) (loop r)
	in
	loop tree

let draw_string x y str =
	let (sx, sy) = Graphics.text_size str in
	Graphics.moveto (x - sx / 2) (y - sy / 2);
	Graphics.draw_string str

let draw_node x y size str =
	draw_square x y size;
	draw_string x y str

let draw_tree tree =
	let rec draw x y acc = function
		| Nil -> draw_node x (y - 50) 50 "Nil"
		| Node(v, l, r) ->
			draw_node x (y - 50) 50 v;
			Graphics.moveto x (y - 75);
			Graphics.lineto (x - 105 - ((heigth l) * 30)) (y - acc - 140);
			draw (x - 105 - ((heigth l) * 30)) (y - acc - 115) (acc + 20) l;
			Graphics.moveto x (y - 75);
			Graphics.lineto (x + 105 + ((heigth l) * 30)) (y - acc - 140);
			draw (x + 105 + ((heigth l) * 30)) (y - acc - 115) (acc + 20) r
	in
	draw 400 600 0 tree

let () =
	Graphics.open_graph " 800x600";
	draw_tree (Node("42", Node("15", Nil, Node("65", Nil, Nil)), Node("22", Nil, Nil)));
	ignore(Graphics.read_key ())
