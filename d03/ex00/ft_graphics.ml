(* ************************************************************************** *)
(*                                                                            *)
(*                                                        :::      ::::::::   *)
(*   ft_graphics.ml                                     :+:      :+:    :+:   *)
(*                                                    +:+ +:+         +:+     *)
(*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        *)
(*                                                +#+#+#+#+#+   +#+           *)
(*   Created: 2016/01/20 21:25:34 by cpestour          #+#    #+#             *)
(*   Updated: 2016/01/20 23:06:51 by cpestour         ###   ########.fr       *)
(*                                                                            *)
(* ************************************************************************** *)

type 'a tree = Nil | Node of 'a * 'a tree * 'a tree

let draw_square x y size =
	Graphics.moveto (x - size / 2) (y - size / 2);
	Graphics.lineto (x - size / 2 + size) (y - size / 2);
	Graphics.lineto (x - size / 2 + size) (y - size / 2 + size);
	Graphics.lineto (x - size / 2) (y - size / 2 + size);
	Graphics.lineto (x - size / 2) (y - size / 2)

let string_of_node = function
	| Nil -> "Nil"
	| Node(v, _, _) -> string_of_int v

let draw_tree_node node = match node with
		| Nil -> ()
		| _ ->
			draw_square 400 500 70;
			Graphics.moveto 400 500;
			Graphics.draw_string (string_of_node node);
			Graphics.moveto 400 465;
			Graphics.lineto 200 300;
			draw_square 200 265 70;
			Graphics.moveto 200 265;
			Graphics.draw_string (string_of_node Nil);
			Graphics.moveto 400 465;
			Graphics.lineto 600 300;
			draw_square 600 265 70;
			Graphics.moveto 600 265;
			Graphics.draw_string (string_of_node Nil)

let main () =
	Graphics.open_graph " 800x600";
	draw_tree_node (Node(42, Nil, Nil));
	Graphics.read_key ()

let () =
	ignore(main ())
