(* ************************************************************************** *)
(*                                                                            *)
(*                                                        :::      ::::::::   *)
(*   btree.ml                                           :+:      :+:    :+:   *)
(*                                                    +:+ +:+         +:+     *)
(*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        *)
(*                                                +#+#+#+#+#+   +#+           *)
(*   Created: 2016/01/22 19:53:44 by cpestour          #+#    #+#             *)
(*   Updated: 2016/01/22 20:19:36 by cpestour         ###   ########.fr       *)
(*                                                                            *)
(* ************************************************************************** *)

type 'a tree = Nil | Node of 'a * 'a tree * 'a tree

let is_bst tree =
	let rec check_l i min max = function
		| Nil -> true
		| Node(v, l, r) ->
			if v < i && v > min then
				(check_l v min v l) && (check_r v v max r)
			else false
	and check_r i min max = function
		| Nil -> true
		| Node(v, l, r) ->
			if v > i && v < max then
				(check_l v min v l) && (check_r v v max r)
			else false
	in match tree with
	| Nil -> true
	| Node(v, l, r) -> (check_l v min_int v l) && (check_r v v max_int r)

let is_perfect tree =
	let rec loop = function
		| Nil -> true
		| Node(_, Node(_, _, _), Nil) | Node(_, Nil, Node(_, _, _)) -> false
		| Node(_, l, r) -> loop l && loop r
	in
	is_bst tree && loop tree

let search_bst n tree =
	let rec loop = function
		| Nil -> false
		| Node(v, l, r) ->
			if n = v then true
			else if n < v then loop l
			else loop r
	in
	loop tree

let () =
	print_endline (string_of_bool (search_bst 15 (Node(42, Node(15, Node(9, Nil, Nil), Node(25, Nil, Nil)), Node(82, Nil, Nil)))));
