(* ************************************************************************** *)
(*                                                                            *)
(*                                                        :::      ::::::::   *)
(*   cipher.ml                                          :+:      :+:    :+:   *)
(*                                                    +:+ +:+         +:+     *)
(*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        *)
(*                                                +#+#+#+#+#+   +#+           *)
(*   Created: 2016/01/22 18:47:15 by cpestour          #+#    #+#             *)
(*   Updated: 2016/01/22 19:50:44 by cpestour         ###   ########.fr       *)
(*                                                                            *)
(* ************************************************************************** *)

let is_lower c = c >= 'a' && c <= 'z'

let is_upper c = c >= 'A' && c <= 'Z'

let rot c =
	if is_lower c then
		char_of_int((int_of_char(c) - int_of_char('a') + 1) mod 26 + int_of_char('a'))
	else if is_upper c then
		char_of_int((int_of_char(c) - int_of_char('A') + 1) mod 26 + int_of_char('A'))
	else
		c

let rec caesar str n =
	if n > 0 then
		caesar (String.map rot str) (n - 1)
	else
		str

let rot42 str = caesar str 42

let xor str key =
	String.map (fun c -> char_of_int ((int_of_char c) lxor key)) str

let ft_crypt : string->(string->int->string)list->string = fun str fns ->
	let rec loop s = function
		| [] -> s
		| h::l -> loop (h s 2) l
	in
	loop str fns
