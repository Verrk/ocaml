(* ************************************************************************** *)
(*                                                                            *)
(*                                                        :::      ::::::::   *)
(*   uncipher.ml                                        :+:      :+:    :+:   *)
(*                                                    +:+ +:+         +:+     *)
(*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        *)
(*                                                +#+#+#+#+#+   +#+           *)
(*   Created: 2016/01/22 19:03:06 by cpestour          #+#    #+#             *)
(*   Updated: 2016/01/22 19:19:13 by cpestour         ###   ########.fr       *)
(*                                                                            *)
(* ************************************************************************** *)

let is_lower c = c >= 'a' && c <= 'z'

let is_upper c = c >= 'A' && c <= 'Z'

let rot c =
	if is_lower c then
		char_of_int((int_of_char(c) - int_of_char('a') + 1) mod 26 + int_of_char('a'))
	else if is_upper c then
		char_of_int((int_of_char(c) - int_of_char('A') + 1) mod 26 + int_of_char('A'))
	else
		c

let uncaesar str n =
	let rec loop str i =
		if i > 0 then
			loop (String.map rot str) (i - 1)
		else
			str
	in
	loop str (26 - (n mod 26))

let unrot42 str = uncaesar str 42
