(* ************************************************************************** *)
(*                                                                            *)
(*                                                        :::      ::::::::   *)
(*   main.ml                                            :+:      :+:    :+:   *)
(*                                                    +:+ +:+         +:+     *)
(*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        *)
(*                                                +#+#+#+#+#+   +#+           *)
(*   Created: 2016/01/22 19:13:25 by cpestour          #+#    #+#             *)
(*   Updated: 2016/01/22 19:37:50 by cpestour         ###   ########.fr       *)
(*                                                                            *)
(* ************************************************************************** *)

let () =
	print_endline (Cipher.ft_crypt "hello" [(Cipher.caesar 12); (Cipher.xor 2)]);
	print_endline (Uncipher.unrot42 (Cipher.rot42 "hello"))
